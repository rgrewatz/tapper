//
//  ViewController.swift
//  Tapper
//
//  Created by Ryan Grewatz on 10/7/16.
//  Copyright © 2016 Ryan Grewatz. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var screen: UIView!
    @IBOutlet var menuViews: [UIView]!
    @IBOutlet weak var tapsToWin: UITextField!
    @IBOutlet weak var tapBtn: UIButton!
    var count: Int = 0
    var buttonText: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonText = tapBtn.title(for: .normal) ?? ""
        // Do any additional setup after loading the view, typically from a nib.        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        tapsToWin.addTarget(self, action: #selector(hideKeyboard), for: UIControlEvents.touchDown)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func hideKeyboard(UICon: NSNotification){
        tapsToWin.endEditing(true)
    }
    func keyboardWillShow(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
            else {
                
            }
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y != 0 {
                self.view.frame.origin.y += keyboardSize.height
            }
            else {
                
            }
        }
    }
    
    @IBAction func startGame(_ sender: AnyObject) {
        swapVisibility()
    }

    private func swapVisibility(){
        menuViews.forEach{
            $0.isHidden = !$0.isHidden
        }
    }
    
    @IBAction func incrementCount(_ sender: AnyObject) {
        count += 1
        tapBtn.setTitle(String(count), for: .normal)
        if let c = tapsToWin.text{
            if count == Int(c){
                count = 0
                tapBtn.setTitle(buttonText, for: .normal)
                swapVisibility()
            }
        }
    }
}

